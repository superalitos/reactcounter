import React, { Component } from "react";

class Counter extends Component {
  componentDidUpdate(prevProps, prevState) {
    console.log("prevProps= ", prevProps);
    console.log("prevState= ", prevState);

    if (prevProps.counter.value !== this.props.counter.value) {
      // make new Ajax call to get new DATA
    }
  }

  componentWillUnmount() {
    console.log("counter - Unmount");
  }

  render() {
    console.log(this.props);
    // {this.props.children}
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">
            <div className="col-1">
              <span className={this.getBadgeClasses()}>
                {this.formatCount()}
              </span>
            </div>
            <div className="col">
              <button
                onClick={() => this.props.onIncrement(this.props.counter)}
                className="btn btn-secondary btn-sm m-2"
              >
                +
              </button>
              <button
                onClick={() => this.props.onDecrease(this.props.counter)}
                className="btn btn-secondary btn-sm m-2"
                disabled={this.props.counter.value === 0 ? "disabled" : ""}
              >
                -
              </button>
              <button
                onClick={() => this.props.onDelete(this.props.counter.id)}
                className="btn btn-danger btn-sm m-2"
              >
                x
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;
